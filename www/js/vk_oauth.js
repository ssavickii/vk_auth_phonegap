// SET INDIVIDUAL APP PARAMS
vkVars = {
  scope: "friends",
  APP_ID: '4695693',
  api_v: 5.27
};

// используется jquery

//-------------------------------------------
var viewer_id;
var access_token;

var url_parser = {

  get_args: function (s) {
    var tmp=new Array();
    s=(s.toString()).split('&');
    for (var i in s) {
      i=s[i].split("=");
      tmp[(i[0])]=i[1];
    }
    return tmp;
  },

  get_args_cookie: function (s) {
    var tmp=new Array();
    s=(s.toString()).split('; ');
    for (var i in s) {
      i=s[i].split("=");
      tmp[(i[0])]=i[1];
    }
    return tmp;
  }

};

var vkOauth = {
  attempts: 0,
  max_attempts: 3,

  wwwref: false,

  scope: vkVars.scope,
  APP_ID: vkVars.APP_ID,

  // точка входа
  auth: function (force) {
    //если в телефоне данные не сохранены
    if (!window.localStorage.getItem("vkOauth_token") || force || window.localStorage.getItem("vkOauth_perms")!=vkOauth.scope) {
      //URL авторизации
      var authURL="https://oauth.vk.com/authorize?client_id=" + vkOauth.APP_ID + "&scope="+this.scope+"&redirect_uri=http://oauth.vk.com/blank.html&display=mobile&response_type=token";
      //Используем inAppBrowser  (window.open)
      this.wwwref = window.open(encodeURI(authURL), '_blank', 'location=no');
      this.wwwref.addEventListener('loadstop', this.auth_event_url);
    } else {
      //если данные есть в телефоне
      <!--    USER_ID VK из localStorage -->
      viewer_id = window.localStorage.getItem("vkOauth_user_id");
      access_token  = window.localStorage.getItem("vkOauth_token")

      if (viewer_id  && access_token) {
        // тестовый запрос на проверку работы токена
        vkOauth.getProfileCheck();
      }
    }
  },

  // точка выхода - здесь начинается необходимая логика для продолжения работы приложения
  outPoint: function () {
    console.log('VK OK! продолжать логику здесь');
  },

  // тестовый запрос - при неудаче авторизуемся с нуля
  getProfileSuccess: function (data) {
    // ERROR - запускаем повторно авторизацию - возможно устарел токен и тп.
    if (data.hasOwnProperty('error')) {
      if (vkOauth.attempts < vkOauth.max_attempts) {

        vkOauth.attempts = vkOauth.attempts + 1;

        // насильная переавторизация (force: true)
        vkOauth.auth(true);
      } else {
        console.log('vkOauth: no have attempts, query fail!');
      }
    } else {
      // SUCCESS
      vkOauth.outPoint();
      console.log(data);
    }
  },

  getProfileCheck: function () {
    $.ajax({ url: 'https://api.vk.com/method/account.getProfileInfo',
      type: 'get',
      dataType: 'jsonp',
      crossDomain: true,
      data: { access_token: localStorage.getItem('vkOauth_token'), v: vkVars.api_v},
      success: vkOauth.getProfileSuccess,
      error: function () {
        console.log('ajax api query error');
      }
    });

  },

  auth_event_url: function (event) {//Когда загрузка в браузере закончилась
    var tmp=(event.url).split("#");
    //Если мы на странице oauth.vk.com/blank.html
    if (tmp[0]=='https://oauth.vk.com/blank.html' || tmp[0]=='http://oauth.vk.com/blank.html') {
      //Закрываем окно
      vkOauth.wwwref.close();
      var tmp=url_parser.get_args(tmp[1]);
      //navigator.notification.alert(tmp['access_token']);
      //Сохраняем токен и user_id ЛОКАЛЬНО
      window.localStorage.setItem("vkOauth_token", tmp['access_token']);
      window.localStorage.setItem("vkOauth_user_id", tmp['user_id']);
      //                            window.localStorage.setItem("vkOauth_exp", tmp['expires_in']); // == 0 - вечный
      window.localStorage.setItem("vkOauth_perms", vkOauth.scope);
      //navigator.notification.alert('Готово!');
      <!--    USER_ID VK после авторизации-->
      access_token = tmp['access_token'];
      viewer_id = tmp['user_id']

      vkOauth.getProfileCheck();


    } else {
      //console.log('это не страница для авторизации вк');
    }
  },

  startPoint: function () {

    var intervalId = setTimeout(function () {
      if (typeof $ !== 'undefined') {
        vkOauth.auth();

        clearInterval(intervalId);
      }
    }, 1000);


  }
};

//=============================================

// START SCRIPT onDeviceReady

// так как используется jquery.ajax
//vkOauth.startPoint();